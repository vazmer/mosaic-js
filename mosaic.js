var lenX = 20,
    lenY = 16,
    maxDistanceBetweenSpots = .5;

var drawingEl = document.getElementById('drawing'),
    drawingElHeight = drawingEl.offsetHeight - 20,
    drawingElWidth = drawingEl.offsetWidth - 20,
    drawingElItemHeight = drawingElHeight / lenY,
    drawingElItemWidth = drawingElWidth / lenX;

// Place the SVG namespace in a variable to easily reference it.
var xmlns = "http://www.w3.org/2000/svg";
var svgElem = document.createElementNS(xmlns, "svg");
svgElem.setAttributeNS(null, "width", drawingElWidth);
svgElem.setAttributeNS(null, "height", drawingElHeight);
drawingEl.appendChild(svgElem);

var src = {};

function addItem(x, y) {
    var topLeftItem = src[(x - 1) + '-' + (y - 1)],
        topItem = src[x + '-' + (y - 1)],
        leftItem = src[(x - 1) + '-' + y];

    var prevY = topItem !== undefined ? topItem.moveY - 1 + maxDistanceBetweenSpots : 0,
        prevX = leftItem !== undefined ? leftItem.moveX - 1 + maxDistanceBetweenSpots : 0;

    var moveX = x === 0 ? 0 : (x === lenX - 1) ? 1 : calculateMove(prevX),
        moveY = y === 0 ? 0 : (y === lenY - 1) ? 1 : calculateMove(prevY);

    var item = {
        moveX: moveX,
        moveY: moveY,
        positionX: x * drawingElItemWidth + (drawingElItemWidth * moveX),
        positionY: y * drawingElItemHeight + (drawingElItemHeight * moveY)
    };

    src[x + '-' + y] = item;

    if(topLeftItem === undefined || topItem === undefined || leftItem === undefined) return;

    var points = [];
    [topLeftItem, topItem, item, leftItem].forEach(function(connectedItem) {
        points.push(connectedItem.positionX + ',' + connectedItem.positionY);
    });

    var polygonEl = document.createElementNS(xmlns, "polygon");
    polygonEl.setAttributeNS(null, "points", points.join(' '));
    polygonEl.setAttributeNS(null, "style", 'transform: matrix(1, ' + getRandomArbitrary(-1, 1) + ', 0, 1, ' + getRandomArbitrary(-1, 1) + ', 0); rotate(-190deg); opacity: 0;');
    polygonEl.setAttributeNS(null, "fill", "#"+((1<<24)*Math.random()|0).toString(16));

    setTimeout(function() {
        polygonEl.setAttributeNS(null, "style", 'opacity:1');
    }, getRandomArbitrary(600, 900));

    svgElem.appendChild(polygonEl);
}

function calculateMove(prevVal) {
    return getRandomArbitrary(prevVal, 1);
}

/**
 * Returns a random number between min (inclusive) and max (exclusive)
 */
function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}


for (var y = 0; y < lenY; y++) {
    for (var x = 0; x < lenX; x++) {
        addItem(x, y);
    }
}

drawingEl.setAttribute('class', 'out');

setTimeout(function() {
    drawingEl.setAttribute('class', '');
}, 500);

